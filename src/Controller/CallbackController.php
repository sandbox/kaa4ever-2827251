<?php

namespace Drupal\quickpay_integration\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\quickpay_integration\Entity\QuickpayIntegration;
use Drupal\quickpay_integration\QuickpayIntegrationTransaction;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

/**
 * Endpoints for the routes defined.
 */
class CallbackController extends ControllerBase {
  /**
   * The logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * CallbackController constructor.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   The logger channel.
   */
  public function __construct(ModuleHandlerInterface $module_handler, LoggerChannelFactoryInterface $logger) {
    $this->moduleHandler = $module_handler;
    $this->logger = $logger->get('quickpay_integration');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('module_handler'),
      $container->get('logger.factory')
    );
  }

  /**
   * Callback from Quickpay.
   *
   * @param string $order_id
   *   The order id used in the transaction.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   A response
   */
  public function callback($order_id, Request $request) {
    $response = new Response();
    $response->setStatusCode(500);
    try {
      $content = json_decode($request->getContent());
      $quickpay = QuickpayIntegration::loadFromRequest($content);
      if ($quickpay) {
        $transaction = new QuickpayIntegrationTransaction($quickpay, $content);
        // Invoke hook_quickpay_callback.
        $this->moduleHandler->invokeAll('quickpay_integration_callback', [$order_id, $transaction]);
        $response->setStatusCode(200);
      }
    }
    catch (\Exception $e) {
      $this->logger->error('Could not create transaction from request: !request.', ['!request' => print_r($request, TRUE)]);
    }
    return $response;
  }

}
