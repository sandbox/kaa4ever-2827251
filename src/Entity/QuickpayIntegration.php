<?php

namespace Drupal\quickpay_integration\Entity;

use Drupal\quickpay_integration\QuickpayIntegrationException;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Language\LanguageInterface;

/**
 * Defines the Quickpay entity.
 *
 * @ConfigEntityType(
 *   id = "configuration",
 *   label = @Translation("Quickpay integration configuration"),
 *   module = "quickpay_integration",
 *   config_prefix = "configuration",
 *   admin_permission = "administer site configuration",
 *   handlers = {
 *     "list_builder" = "Drupal\quickpay_integration\QuickpayIntegrationListBuilder",
 *     "form" = {
 *       "default" = "Drupal\quickpay_integration\Form\ConfigurationForm",
 *       "delete" = "Drupal\quickpay_integration\Form\ConfigurationDeleteForm"
 *     },
 *   },
 *   links = {
 *     "edit-form" = "/admin/config/quickpay/manage/{configuration}",
 *     "delete-form" = "/admin/config/quickpay/manage/{configuration}/delete"
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label"
 *   }
 * )
 */
class QuickpayIntegration extends ConfigEntityBase implements QuickpayIntegrationInterface {
  const QUICKPAY_VERSION = 'v10';
  /**
   * The unique ID of the Quickpay configuration.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the Quickpay configuration.
   *
   * @var string
   */
  public $label;

  /**
   * The description of the Quickpay configuration.
   *
   * @var string
   */
  public $description;

  /**
   * The merchant ID from Quickpay.
   *
   * @var string
   */
  public $merchant_id;

  /**
   * The agreement ID from Quickpay.
   *
   * @var string
   */
  public $agreement_id;

  /**
   * The API key from Quickpay.
   *
   * @var string
   */
  public $api_key;

  /**
   * The private key from Quickpay.
   *
   * @var string
   */
  public $private_key;

  /**
   * Prefix to prepend to the order ID.
   *
   * @var string
   */
  public $orderPrefix = '';

  /**
   * Language for the card information form.
   *
   * @var string
   */
  public $language = LanguageInterface::LANGCODE_NOT_SPECIFIED;

  /**
   * The payment method to use.
   *
   * @var string
   */
  public $payment_method = 'creditcard';

  /**
   * Array of accepted cards.
   *
   * @var array
   */
  public $accepted_cards = ['creditcard'];

  /**
   * Whether autofee should be enabled.
   *
   * @var bool
   */
  public $autofee = FALSE;

  /**
   * Whether auto capture should be enabled.
   *
   * @var bool
   */
  public $autocapture = FALSE;

  /**
   * Whether to activate debug mode, logging all interations.
   *
   * @var bool
   */
  public $debug = FALSE;

  /**
   * Get the language of the user.
   *
   * @inheritdoc
   *
   * @TODO: If LanguageInterface::LANGCODE_NOT_SPECIFIED the current users
   * language should be used and not 'en'.
   */
  public function getLanguage() {
    $language_code = LanguageInterface::LANGCODE_NOT_SPECIFIED ? 'en' : $this->language;
    $language_codes = [
      'da' => 'da',
      'de' => 'de',
      'en' => 'en',
      'fo' => 'fo',
      'fr' => 'fr',
      'kl' => 'gl',
      'it' => 'it',
      'nb' => 'no',
      'nn' => 'no',
      'nl' => 'nl',
      'pl' => 'pl',
      'sv' => 'se',
    ];
    return isset($language_codes[$language_code]) ? $language_codes[$language_code] : 'en';
  }

  /**
   * Get information about an currency.
   *
   * @inheritdoc
   */
  public function currencyInfo($code) {
    // If the currency is not known, throw an exception.
    if (!array_key_exists($code, $this->getBaseCurrencies())) {
      throw new QuickpayIntegrationException(t('Unknown currency code %currency', ['%currency' => $code]));
    }
    $base_currencies = $this->getBaseCurrencies();
    return $base_currencies[$code];
  }

  /**
   * A map of currencies know by the module.
   *
   * @inheritdoc
   */
  private function getBaseCurrencies() {
    return [
      'DKK' => ['code' => 'DKK', 'multiplier' => 100],
      'USD' => ['code' => 'USD', 'multiplier' => 100],
      'EUR' => ['code' => 'EUR', 'multiplier' => 100],
      'GBP' => ['code' => 'GBP', 'multiplier' => 100],
      'SEK' => ['code' => 'SEK', 'multiplier' => 100],
      'NOK' => ['code' => 'NOK', 'multiplier' => 100],
      'ISK' => ['code' => 'ISK', 'multiplier' => 100],
    ];
  }

  /**
   * Returns the amount adjusted by the multiplier for the currency.
   *
   * @inheritdoc
   */
  public function wireAmount($amount, array $currency_info) {
    return (function_exists('bcmul') ? bcmul($amount, $currency_info['multiplier']) : $amount * $currency_info['multiplier']);
  }

  /**
   * Reverses wireAmount().
   *
   * @inheritdoc
   */
  public function unwireAmount($amount, array $currency_info) {
    return (function_exists('bcdiv') ?
      bcdiv($amount, $currency_info['multiplier'], log10($currency_info['multiplier'])) :
      $amount / $currency_info['multiplier']);
  }

  /**
   * Return the proper cardtypelock for the accepted cards.
   *
   * @inheritdoc
   */
  public function getPaymentMethods() {
    if (is_array($this->accepted_cards)) {
      $cards = $this->accepted_cards;
      // Filter out all cards not selected.
      $cards = array_filter($cards, function ($is_selected) {
          return $is_selected;
      }, ARRAY_FILTER_USE_BOTH);
      // Aren't supported in cardtypelock.
      unset($cards['ikano']);
      return implode(',', $cards);
    }
    // Already set to the proper string.
    return $this->accepted_cards;
  }

  /**
   * Calculate the md5checksum for the request.
   *
   * @see http://tech.quickpay.net/payments/hosted/#checksum
   *
   * @inheritdoc
   */
  public function getChecksum(array $data) {
    ksort($data);
    $base = implode(" ", $data);
    return hash_hmac("sha256", $base, $this->api_key);
  }

  /**
   * Build the checksum from the request callback from quickpay.
   *
   * @inheritdoc
   */
  public function getChecksumFromRequest($request) {
    return hash_hmac("sha256", $request, $this->private_key);
  }

  /**
   * Request a QuickPay service.
   *
   * @see http://tech.quickpay.net/api/services/?scope=merchant
   *
   * @inheritdoc
   */
  public function request($url) {
    $client = \Drupal::httpClient();
    try {
      $response = $client->get($url, [
        'auth' => ['', $this->get('api_key')],
        'headers' => [
          'Accept-Version' => self::QUICKPAY_VERSION,
        ],
      ]
      );
      return json_decode($response->getBody());
    }
    catch (\Exception $e) {
      throw new QuickpayIntegrationException($e->getMessage());
    }
  }

  /**
   * Initialize a Quickpay instance from a request.
   *
   * @inheritdoc
   */
  public static function loadFromRequest($request) {
    if (!isset($request->variables->quickpay_configuration_id)) {
      \Drupal::logger('quickpay')->error(t('QuickPay configuration could not be loaded from request. No quickpay_configuration_id variable set.'));
      return FALSE;
    }
    $configuration_id = $request->variables->quickpay_configuration_id;
    return self::load($configuration_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getMerchantId() {
    return $this->merchant_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getAgreementId() {
    return $this->agreement_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getOrderPrefix() {
    return $this->orderPrefix;
  }

  /**
   * {@inheritdoc}
   */
  public function getAutoCapture() {
    return $this->autocapture;
  }

  /**
   * {@inheritdoc}
   */
  public function getAutoFee() {
    return $this->autofee;
  }

}
