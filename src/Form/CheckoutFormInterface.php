<?php

namespace Drupal\quickpay_integration\Form;

/**
 * Interface CheckoutFormInterface.
 *
 * @package Drupal\quickpay_integration\Form
 */
interface CheckoutFormInterface {

  /**
   * Get the Quickpay integration implementation object.
   *
   * @return \Drupal\quickpay_integration\Entity\QuickpayIntegrationInterface
   *   The object.
   */
  public function getQuickpayIntegration();

  /**
   * Get the order id to use for transaction.
   *
   * @return string
   *   The order id.
   */
  public function getOrderId();

  /**
   * Get currency to use for transaction.
   *
   * @return string
   *   The currency code.
   */
  public function getCurrency();

  /**
   * Get the amount to transact.
   *
   * @return float
   *   The amount to withdraw.
   */
  public function getAmount();

  /**
   * Get url to continue to after success.
   *
   * @return string
   *   An absolute url.
   */
  public function getContinueUrl();

  /**
   * Get url to fallback to on error or exit.
   *
   * @return string
   *   An absolute url.
   */
  public function getCancelUrl();

}
