<?php

namespace Drupal\quickpay_integration\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\quickpay_integration\Entity\QuickpayIntegration;

/**
 * The checkout form for redirect users to QuickPay for payment.
 *
 * This is an abstract class, since custom modules must extend this form
 * in their own checkout forms.
 * Add custom variables to send to quickpay by adding them to $this->_custom.
 *
 * @TODO: Add possibility for other message types.
 */
abstract class CheckoutForm extends FormBase implements CheckoutFormInterface {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Build the form.
    $form['#method'] = 'POST';
    $form['#action'] = 'https://payment.quickpay.net';
    // Required variables.
    $data['version'] = QuickpayIntegration::QUICKPAY_VERSION;
    $data['merchant_id'] = $this->getQuickpayIntegration()->getMerchantId();
    $data['agreement_id'] = $this->getQuickpayIntegration()->getAgreementId();
    $data['order_id'] = $this->getOrderId();
    // Ensure that Order number is at least 4 characters. Else Quickpay will
    // reject the request.
    if (strlen($data['order_id']) < 4) {
      $data['order_id'] = $this->getQuickpayIntegration()->getOrderPrefix() . substr('0000' . $this->getOrderId(), -4 + strlen($this->getQuickpayIntegration()->getOrderPrefix()));
    }
    $currency_info = $this->getQuickpayIntegration()->currencyInfo($this->getCurrency());
    $data['amount'] = $this->getQuickpayIntegration()->wireAmount($this->getAmount(), $currency_info);
    $data['currency'] = $currency_info['code'];
    $data['continueurl'] = $this->getContinueUrl();
    $data['cancelurl'] = $this->getCancelUrl();
    $data['callbackurl'] = Url::fromRoute('quickpay_integration.callback', ['order_id' => $this->getOrderId()], ['absolute' => TRUE])->toString();
    // Set the optional variables.
    $data['language'] = $this->getQuickpayIntegration()->getLanguage();
    $data['autocapture'] = $this->getQuickpayIntegration()->getAutoCapture() ? '1' : '0';
    $data['payment_methods'] = $this->getQuickpayIntegration()->getPaymentMethods();
    $data['autofee'] = $this->getQuickpayIntegration()->getAutoFee() ? '1' : '0';
    // Add the ID of the Quickpay configuration as a custom variable to load it
    // in the response.
    $data['variables[quickpay_configuration_id]'] = $this->getQuickpayIntegration()->id();
    // Add any custom fields.
    if (isset($this->_custom) && is_array($this->_custom)) {
      foreach ($this->_custom as $key => $value) {
        $data['variables[' . $key . ']'] = $value;
      }
    }
    // Build the checksum.
    $data['checksum'] = $this->getQuickpayIntegration()->getChecksum($data);
    // Add all data elements as hidden input fields.
    foreach ($data as $name => $value) {
      $form[$name] = ['#type' => 'hidden', '#value' => $value];
    }
    $form['submit'] = [
      '#id' => 'quickpay-submit-button',
      '#type' => 'submit',
      '#value' => $this->t('Continue to QuickPay'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

}
