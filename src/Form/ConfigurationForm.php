<?php

namespace Drupal\quickpay_integration\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;

/**
 * Class ConfigurationForm.
 *
 * @package Drupal\quickpay_integration\Form
 */
class ConfigurationForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\quickpay_integration\Entity\QuickpayIntegration $entity */
    $entity = $this->entity;

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Configuration name'),
      '#default_value' => $entity->label(),
      '#size' => 30,
      '#required' => TRUE,
      '#maxlength' => 64,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $entity->id(),
      '#required' => TRUE,
      '#disabled' => !$entity->isNew(),
      '#size' => 30,
      '#maxlength' => 64,
      '#machine_name' => [
        'exists' => ['\Drupal\quickpay_integration\Entity\QuickpayIntegration', 'load'],
      ],
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Configuration description'),
      '#description' => $this->t('Where the configuration is used, usecases, etc.'),
      '#default_value' => $entity->get('description'),
      '#required' => TRUE,
    ];

    $form['merchant_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchant ID'),
      '#description' => $this->t('This is the Merchant ID from the Quickpay manager.'),
      '#default_value' => $entity->get('merchant_id'),
      '#required' => TRUE,
    ];

    $form['private_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Private'),
      '#description' => $this->t('This is the private key from the Quickpay manager.'),
      '#default_value' => $entity->get('private_key'),
      '#required' => TRUE,
    ];

    $form['agreement_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Agreement ID'),
      '#description' => $this->t('The agreement ID for the QuickPay user which will be used when going through payment. This will typically be the "Payment Window" user.'),
      '#default_value' => $entity->get('agreement_id'),
      '#required' => TRUE,
    ];

    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API'),
      '#description' => $this->t('The API key for the same user as used in Agreement ID.'),
      '#default_value' => $entity->get('api_key'),
      '#required' => TRUE,
    ];

    $form['order_prefix'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Order ID prefix'),
      '#description' => $this->t('Prefix for order IDs. Order IDs must be uniqe when sent to QuickPay, use this to resolve clashes.'),
      '#default_value' => $entity->get('order_prefix'),
    ];

    $languages = $this->getLanguages() + [LanguageInterface::LANGCODE_NOT_SPECIFIED => $this->t('Language of the user')];
    $form['language'] = [
      '#type' => 'select',
      '#title' => $this->t('Language'),
      '#description' => $this->t('The language for the credit card form.'),
      '#options' => $languages,
      '#default_value' => $entity->get('language'),
    ];

    $form['payment_method'] = [
      '#type' => 'radios',
      '#id' => 'quickpay-method',
      '#title' => $this->t('Accepted payment methods'),
      '#description' => $this->t('Which payment methods to accept. NOTE: Some require special agreements.'),
      '#default_value' => $entity->get('payment_method'),
      '#options' => [
        'creditcard' => $this->t('Creditcard'),
        '3d-creditcard' => $this->t('3D-Secure Creditcard'),
        'selected' => $this->t('Selected payment methods'),
      ],
    ];

    $options = [];
    // Add image to the cards where defined.
    foreach ($this->getQuickpayCards() as $key => $card) {
      $options[$key] = empty($card['image']) ? $card['name'] : '<img src="/' . $card['image'] . '" />' . $card['name'];
    }

    $form['accepted_cards'] = [
      '#type' => 'checkboxes',
      '#id' => 'quickpay-cards',
      '#title' => $this->t('Select accepted cards'),
      '#default_value' => $entity->get('accepted_cards'),
      '#options' => $options,
      '#states' => [
        'visible' => [
          ':input[name="payment_method"]' => ['value' => 'selected'],
        ],
      ],
    ];

    $form['autofee'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Autofee'),
      '#description' => $this->t('If set, the fee charged by the acquirer will be calculated and added to the transaction amount.'),
      '#default_value' => $entity->get('autofee'),
    ];

    $form['autocapture'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Autocapture'),
      '#description' => $this->t('If set, the transactions will be automatically captured.'),
      '#default_value' => $entity->get('autocapture'),
    ];

    $form['debug'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Debug log'),
      '#description' => $this->t('Log all request and responses to QuickPay in the Watchdog.'),
      '#default_value' => $entity->get('debug'),
    ];

    return parent::form($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\quickpay_integration\Entity\QuickpayIntegration $entity */
    $entity = $this->entity;

    // Prevent leading and trailing spaces.
    $entity->set('label', trim($entity->label()));
    $entity->set('description', $form_state->getValue('description'));
    $entity->set('merchant_id', $form_state->getValue('merchant_id'));
    $entity->set('agreement_id', $form_state->getValue('agreement_id'));
    $entity->set('api_key', $form_state->getValue('api_key'));
    $entity->set('private_key', $form_state->getValue('private_key'));
    $entity->set('order_prefix', $form_state->getValue('order_prefix'));
    $entity->set('language', $form_state->getValue('language'));
    $entity->set('payment_method', $form_state->getValue('payment_method'));
    if ($form_state->getValue('payment_method') === 'selected') {
      $entity->set('accepted_cards', $form_state->getValue('accepted_cards'));
    }
    else {
      $entity->set('accepted_cards', [$form_state->getValue('payment_method')]);
    }
    $entity->set('autofee', $form_state->getValue('autofee'));
    $entity->set('debug', $form_state->getValue('debug'));
    $status = $entity->save();

    $edit_link = $entity->link($this->t('Edit'));
    $action = $status == SAVED_UPDATED ? 'updated' : 'added';

    drupal_set_message($this->t('Quickpay configuration %label has been %action.', ['%label' => $entity->label(), '%action' => $action]));
    $this->logger('quickpay')->notice('Quickpay configuration %label has been %action.', ['%label' => $entity->label(), 'link' => $edit_link]);

    $form_state->setRedirect('quickpay_integration.configuration_list');
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);
    $actions['submit']['#value'] = ($this->entity->isNew()) ? $this->t('Add configuration') : $this->t('Update configuration');
    return $actions;
  }

  /**
   * Returns an array of languages supported by Quickpay.
   *
   * @return array
   *   Array with key being language codes, and value being names.
   */
  protected function getLanguages() {
    return [
      'da' => $this->t('Danish'),
      'de' => $this->t('German'),
      'en' => $this->t('English'),
      'fo' => $this->t('Faeroese'),
      'fr' => $this->t('French'),
      'gl' => $this->t('Greenlandish'),
      'it' => $this->t('Italian'),
      'no' => $this->t('Norwegian'),
      'nl' => $this->t('Dutch'),
      'pl' => $this->t('Polish'),
      'se' => $this->t('Swedish'),
    ];
  }

  /**
   * Information about all supported cards.
   *
   * @return array
   *   Array with card name and image.
   */
  protected function getQuickpayCards() {
    $images_path = drupal_get_path('module', 'quickpay_integration') . '/images/';
    return [
      'dankort' => [
        'name' => $this->t('Dankort'),
        'image' => $images_path . 'dan.jpg',
      ],
      'edankort' => [
        'name' => $this->t('eDankort'),
        'image' => $images_path . 'edan.jpg',
      ],
      'visa' => [
        'name' => $this->t('Visa'),
        'image' => $images_path . 'visa.jpg',
      ],
      'visa-dk' => [
        'name' => $this->t('Visa, issued in Denmark'),
        'image' => $images_path . 'visa.jpg',
      ],
      '3d-visa' => [
        'name' => $this->t('Visa, using 3D-Secure'),
        'image' => $images_path . '3d-visa.gif',
      ],
      '3d-visa-dk' => [
        'name' => $this->t('Visa, issued in Denmark, using 3D-Secure'),
        'image' => $images_path . '3d-visa.gif',
      ],
      'visa-electron' => [
        'name' => $this->t('Visa Electron'),
        'image' => $images_path . 'visaelectron.jpg',
      ],
      'visa-electron-dk' => [
        'name' => $this->t('Visa Electron, issued in Denmark'),
        'image' => $images_path . 'visaelectron.jpg',
      ],
      '3d-visa-electron' => [
        'name' => $this->t('Visa Electron, using 3D-Secure'),
      ],
      '3d-visa-electron-dk' => [
        'name' => $this->t('Visa Electron, issued in Denmark, using 3D-Secure'),
      ],
      'mastercard' => [
        'name' => $this->t('Mastercard'),
        'image' => $images_path . 'mastercard.jpg',
      ],
      'mastercard-dk' => [
        'name' => $this->t('Mastercard, issued in Denmark'),
        'image' => $images_path . 'mastercard.jpg',
      ],
      'mastercard-debet-dk' => [
        'name' => $this->t('Mastercard debet card, issued in Denmark'),
        'image' => $images_path . 'mastercard.jpg',
      ],
      '3d-mastercard' => [
        'name' => $this->t('Mastercard, using 3D-Secure'),
      ],
      '3d-mastercard-dk' => [
        'name' => $this->t('Mastercard, issued in Denmark, using 3D-Secure'),
      ],
      '3d-mastercard-debet-dk' => [
        'name' => $this->t('Mastercard debet, issued in Denmark, using 3D-Secure'),
      ],
      '3d-maestro' => [
        'name' => $this->t('Maestro'),
        'image' => $images_path . '3d-maestro.gif',
      ],
      '3d-maestro-dk' => [
        'name' => $this->t('Maestro, issued in Denmark'),
        'image' => $images_path . '3d-maestro.gif',
      ],
      'jcb' => [
        'name' => $this->t('JCB'),
        'image' => $images_path . 'jcb.jpg',
      ],
      '3d-jcb' => [
        'name' => $this->t('JCB, using 3D-Secure'),
        'image' => $images_path . '3d-jcb.gif',
      ],
      'diners' => [
        'name' => $this->t('Diners'),
        'image' => $images_path . 'diners.jpg',
      ],
      'diners-dk' => [
        'name' => $this->t('Diners, issued in Denmark'),
        'image' => $images_path . 'diners.jpg',
      ],
      'american-express' => [
        'name' => $this->t('American Express'),
        'image' => $images_path . 'amexpress.jpg',
      ],
      'american-express-dk' => [
        'name' => $this->t('American Express, issued in Denmark'),
        'image' => $images_path . 'amexpress.jpg',
      ],
      'danske-dk' => [
        'name' => $this->t('Danske Netbetaling'),
        'image' => $images_path . 'danskebank.jpg',
      ],
      'nordea-dk' => [
        'name' => $this->t('Nordea Netbetaling'),
        'image' => $images_path . 'nordea.jpg',
      ],
      'fbg1886' => [
        'name' => $this->t('Forbrugsforeningen'),
        'image' => $images_path . 'forbrugsforeningen.gif',
      ],
      'ikano' => [
        'name' => $this->t('Ikano'),
        'image' => $images_path . 'ikano.jpg',
      ],
      'paypal' => [
        'name' => $this->t('PayPal'),
        'image' => $images_path . 'paypal.jpg',
      ],
      'sofort' => [
        'name' => $this->t('Sofort'),
        'image' => $images_path . 'sofort.png',
      ],
      'viabill' => [
        'name' => $this->t('ViaBill'),
        'image' => $images_path . 'viabill.png',
      ],
    ];
  }

}
