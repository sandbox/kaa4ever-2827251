<?php

namespace Drupal\quickpay_integration;

/**
 * A custom Quickpay exception.
 */
class QuickpayIntegrationException extends \Exception {}
