<?php

namespace Drupal\quickpay_integration\Access;

use Drupal\Core\Routing\Access\AccessInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Access\AccessResult;
use Drupal\quickpay_integration\Entity\QuickpayIntegration;
use Psr\Log\LoggerInterface;

/**
 * Checks access for displaying configuration translation page.
 */
class QuickpayIntegrationCallbackAccessCheck implements AccessInterface {

  /**
   * Logger interface.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * QuickpayIntegrationCallbackAccessCheck constructor.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   Logger interface.
   */
  public function __construct(LoggerInterface $logger) {
    $this->logger = $logger;
  }

  /**
   * Access callback to check that the url parameters hasn't been tampered with.
   *
   * @param string $order_id
   *   The order ID to check access for.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The Symfony request.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   The result of the access check.
   */
  public function access($order_id, Request $request) {
    $content = json_decode($request->getContent());
    $quickpay = QuickpayIntegration::loadFromRequest($content);
    if ($quickpay) {
      $checksum_calculated = $quickpay->getChecksumFromRequest($request->getContent());
      $checksum_requested = $request->server->get('HTTP_QUICKPAY_CHECKSUM_SHA256');
      if (!empty($checksum_requested) && strcmp($checksum_calculated, $checksum_requested) === 0) {
        return AccessResult::allowed();
      }
      $this->logger->error('Computed checksum does not match header checksum.');
    }
    else {
      $this->logger->error('Could not load a Quickpay configuration from request.');
    }
    return AccessResult::forbidden();
  }

}
