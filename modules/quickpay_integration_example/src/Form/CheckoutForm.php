<?php

namespace Drupal\quickpay_integration_example\Form;

use Drupal\Core\Url;
use Drupal\quickpay_integration\Form\CheckoutForm as QuickpayCheckoutForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\NodeInterface;
use Drupal\quickpay_integration\Entity\QuickpayIntegration;

/**
 * Checkout form.
 */
class CheckoutForm extends QuickpayCheckoutForm {
  protected $orderId;
  protected $cancelUrl;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'quickpay_integration_example_checkout';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuickpayIntegration() {
    return QuickpayIntegration::load('example');
  }

  /**
   * {@inheritdoc}
   */
  public function getOrderId() {
    return $this->orderId;
  }

  /**
   * {@inheritdoc}
   */
  public function getAmount() {
    return 1;
  }

  /**
   * {@inheritdoc}
   */
  public function getCurrency() {
    return 'DKK';
  }

  /**
   * {@inheritdoc}
   */
  public function getContinueUrl() {
    return Url::fromRoute('quickpay_integration_example.success', [], ['absolute' => TRUE])->toString();
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return $this->cancelUrl;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, NodeInterface $node = NULL) {
    $this->orderId = time() . '-' . $node->id();
    $this->cancelUrl = Url::fromRoute('quickpay_integration_example.checkout', ['node' => $node->id()], ['absolute' => TRUE])->toString();
    $form = parent::buildForm($form, $form_state);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

}
