<?php

namespace Drupal\quickpay_integration_example\Controller;

use Drupal\node\NodeInterface;
use Drupal\quickpay_integration\Entity\QuickpayIntegration;
use Drupal\quickpay_integration\QuickpayIntegrationTransaction;

/**
 * Endpoints for the routes defined in the test module.
 */
class QuickpayExampleController {

  /**
   * Content for the confirmation page.
   *
   * @return array
   *   A renderable array.
   */
  public function success() {
    return [
      '#type' => 'markup',
      '#markup' => '<h1>Congratulations</h1><p>You just completed a purchase.</p>',
    ];
  }

  /**
   * Display the status for the transaction.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The order node to check for.
   *
   * @return array
   *   A renderable array.
   */
  public function status(NodeInterface $node) {
    if ($transaction_id = $node->get('field_example_transaction_id')->getValue()) {
      $quickpay = QuickpayIntegration::load('example');
      $transaction = new QuickpayIntegrationTransaction($quickpay, $transaction_id);
      $message = 'Approved: ' . ($transaction->approved ? 'TRUE' : 'FALSE') . '</p><p>Message: ' . $transaction->qp_status_msg;
    }
    else {
      $message = 'Transaction has not been completed.';
    }
    return [
      '#type' => 'markup',
      '#markup' => '<p>' . $message . '</p>',
    ];
  }

}
