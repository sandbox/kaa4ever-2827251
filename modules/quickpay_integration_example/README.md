# Quickpay Integration Example
This module serves as an example of how setup an integration with Quickpay.

1. Install the module

2. Go to the Quickpay configurations setup page (/admin/config/quickpay), and 
edit the __Example configuration__ configuration created upon install.  
Update the configuration with your `Merchant ID`, `Agreement ID`, `API` 
and `Private` provided by Quickpay.

3. Create a __Quickpay example order__ node.

4. Open up a browser, and go to __/quickpay_example/checkout/1__, where the 
number `1` should be replaced with the node ID of the node you in step 3.

5. Go through the Quickpay payment flow. You can find test information 
[here](https://tech.quickpay.net/appendixes/test/).

Once payment is completed, edit the node you created, and notice that the 
__transaction id__ field has been updated.   
This was updated in the hook implemented in 
`quickpay_integration_example.module`.
