INTRODUCTION
------------

Quickpay Integration lets you integrate with the danish payment provider, 
Quickpay, in an easy way. 

 * For a full description of the module, visit the project page:
   https://www.drupal.org/sandbox/kaa4ever/2827251

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/2827251
   
REQUIREMENTS
------------

There are no requirements to use this module.
 
INSTALLATION
------------
 
Install as you would normally install a contributed Drupal module.
Se Drupal docs for further information.
   
CONFIGURATION
-------------

When enabling this module, it will have no initial impact on your site.
However, you can access the Configuration page at Configuration » Quickpay 
configurations, and start setting up Quickpay configurations.

For a practically example of how to use the module, please install the
__Quickpay Integration: Example__ module and refer to its __README__ for
further instructions.

When the module is uninstalled, everything will be reverted to before the 
installation.

MAINTAINERS
-----------

Current maintainers:
 * Kristian Kaa (kaa4ever) - https://www.drupal.org/u/kaa4ever 
